// nodemon ./dist/tasks/process-products-desc.js
import path from 'path'
import fs, { Dirent } from 'fs'
import { promisify, callbackify } from 'util'

// promisify(fs.readdir)('').then()

// locate products data dir
const dirPath = path.join(__dirname, '../../data/products')
const outDirPath = path.join(__dirname, '../../data/outdir')
console.log(dirPath)


  /* Async IIFC */
  ; (async () => {
    const res = []
    try {
      // list directories
      const dirs = await fs.promises.readdir(dirPath, {
        withFileTypes: true,
        encoding: 'utf8'
      })
      // const res = await Promise.all(dirs.map(dir => processDir(dir)))
      for (let dir of dirs) {
        res.push(await processDir(dir))
      }

    } catch (err) { throw err }
    finally {
      console.log(res)
    }

  })().catch(console.error)


async function processDir(dir: Dirent) {
  // try {
  if (!dir.isDirectory()) { throw new Error('not a dir') }

  const filePath = path.join(dirPath, dir.name, 'description.md')
  const outFilePath = path.join(outDirPath, dir.name, 'description.md')

  const fileContent = await fs.promises.readFile(filePath, { encoding: 'utf8' })

  const replacedFileContent = fileContent.replace(/^#\s.*?$/gm, '')

  await fs.promises.mkdir(path.join(outDirPath, dir.name), { recursive: true })
  await fs.promises.writeFile(outFilePath, replacedFileContent)

  return outFilePath
  // } catch (err) {
  //   throw (err)
  // }
}



// import http from 'http'

// http.createServer((req,res)=>{
//   res.write('hello')
// }).listen(8080)


