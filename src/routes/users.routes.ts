
import * as express from 'express';
import { UserDTOs, UserListError, UserListResponse, UserQueryParams } from '../interfaces/user.interfaces';

export const userRoutes = express.Router()

import joi from 'joi'
import { type } from 'os';

const querySchema = joi.object({
  filter: joi.string().required(),
  limit: joi.string().default(10),
  offset: joi.string().default(0),
})

const usersList = [
  {
    id: '123',
    name: 'User 243', active: true,
  },
  {
    id: '123',
    name: 'User 123', active: true,
  },
]

userRoutes.get<{}, UserListResponse | UserListError, null, UserQueryParams>('/', (req, res) => {
  let { filter/* , limit = '10', offset = '0' */ } = req.query
  const limit = parseInt(req.query.limit || '10')
  const offset = parseInt(req.query.offset || '10')

  const { error, value } = querySchema.validate(req.query)

  if (error) {
    return res.status(400).send({ error: error.message })
  }

  const userResponse = {
    items: usersList,
    filter,
    limit,
    offset,
  };

  res.send(userResponse)
})


userRoutes.get<null, UserDTOs.User, null, null>('/me', (req, res) => {
  res.send({
    id: '123', name: '123', active: true
  })
})

userRoutes.get<{ id: string }, UserDTOs.User>("/:id", function (req, res) {
  res.send({
    id: '123', name: '123', active: true
  });
});


type CreateUserDTO = Omit<UserDTOs.User, 'id' | 'active'> & { password: string }


const createUserSchema = joi.object({
  name: joi.string().required(),
  password: joi.string().required()
})

userRoutes.post<{}, UserDTOs.User | UserListError, CreateUserDTO>('/', (req, res) => {
  const { error, value } = createUserSchema.validate(req.body)

  if (error) {
    return res.status(400).send({ error: error.message })
  }

  const createdUser = {
    id: Date.now().toString(),
    active: false,
    ...value
  };
  usersList.push(createdUser)

  const { password, ...userWithoutPassword } = createdUser
  res.status(201).send(userWithoutPassword)
})

// type userKeys = 'id' | 'name' | 'profile'
// type UserSansId = {
// [key in userKeys]: string
// [key in keyof CreateUserDTO]?: CreateUserDTO[key]
// readonly [key in keyof CreateUserDTO]: CreateUserDTO[key]
// }

// type Partial<T> = {
//   [key in keyof T]?: T[key]
// }

// type UserSansId = Pick<UserDTOs.User,'id'>
type UserSansId = Omit<UserDTOs.User, 'id'>

// const obj: UserSansId = {
// name
// }

// userRoutes.post('/register', (req, res) => {

// })


userRoutes.get('/calculate', (req, res) => {

  const date = Date.now() + 10_000
  // while (Date.now() < date) { }

  const handle = setInterval(() => {
    console.log('working hard...')

    if (Date.now() > date) {
      clearInterval(handle)
      res.end('<h1>Bye bye! </h1>')
    } else {
      res.flushHeaders()
      res.write('<h1>Hello you </h1>')
    }
  }, 0)

})

