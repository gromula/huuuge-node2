
import { Router } from 'express';


export const couponsRoutes = Router()

couponsRoutes.get('/', (req, res) => {
  const { filter, limit, offset } = req.query

  res.send({
    items: [
        {
            code : "dskhlkwejsglks",
            discount: 0.15
        },
        {
            code : "454ge54te",
            discount: 0.1
        },
    ],
    filter,
    limit, 
    offset
  })
})

couponsRoutes.get("/:code", function (req, res) {
    res.send({
        code : "dskhlkwejsglks",
        discount: 0.15
    });
  });