
import { Router } from 'express';
import { Wish, WishListResponse } from '../interfaces/wishlist.interfaces';

export const wishListRoutes = Router()


    .get<{}, WishListResponse, null, null>('/', async (req, res) => {
        const wishList = {
            items: [
              {
                id: 1,
                name: 'Wishlist 1',
                desc: "123"
              },
              {
                id: 1,
                name: 'Wishlist 2',
                desc: "123"
              },
            ]
          };
        
        
        res.json(wishList)
    })
    .get<{}, Wish, null, null>('/:id', async (req, res) => {


        const wish = {
            id: 1,
            name: 'Wishlist 1',
            desc: "123"
        }

        res.json(wish);
    })