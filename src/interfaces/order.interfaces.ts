export interface productDTO {

}

export interface orderDTO {
    id: number
    products: productDTO[]
}

export interface ordersDTO {
    orders: orderDTO[]
}