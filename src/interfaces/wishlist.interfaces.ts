export interface Wish {
  id: number
  name: string;
  desc: string;
}

export interface WishListResponse {
  items: Wish[];
}
export interface WishListParams {
  id: number
}