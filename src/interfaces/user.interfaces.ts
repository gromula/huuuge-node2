
export interface UserQueryParams {
  filter?: string,
  limit?: string,
  offset?: string
}

export namespace UserDTOs {
  export interface User {
    id: string;
    name: string;
    profile?: {
      fullname: string,
      avatarUrl: string
      birthday?: Date
    }
    active: boolean
  }
}

export interface ErrorResponse { error: string }
export interface UserListError extends ErrorResponse { }

export interface UserListResponse {
  items?: UserDTOs.User[];
  filter?: string;
  limit?: number;
  offset?: number
}

const res: UserListResponse = {};

// if (res.items) {
//   res.items.length;
// } else {
//   res.items
// }

// res.items && res.items.length;
// res.items?.length;
// res.items!.length;

// const x: string | number;


interface Point { x: number; y: number }
interface Vector { x: number; y: number, length: number }

let z: any = {}
let x: Point = { x: 123, y: 124 }
let y: Vector = { x: 123, y: 124, length: 123 }

// x = y
// y = x

// ;(x as Vector).length.toString


// const Xnspc = (() => {

//   const x = 1
//   const y = 1


//   return {
//     y
//   }
// })()
